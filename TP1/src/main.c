/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input;

    hw_Init();

    // Superloop hasta que se presione la tecla Esc
    while (input != EXIT) {
        printf("Ingrese el sensor a activar:\n\n");
        hw_Pausems(2000);
        input = hw_LeerEntrada();
        switch(input)
        {
            case SENSOR_1:
                    hw_AbrirBarrera();
                    hw_Pausems(2000);
                    input = hw_LeerEntrada();
                    if(input == SENSOR_2)
                    { 
                        hw_Pausems(2000);
                        input = hw_LeerEntrada();
                        if(input == SENSOR_1)
                        {
                            hw_AbrirBarrera();
                    
                        }
                        hw_Pausems(5000);
                        hw_CerrarBarrera();
                    }
                    break;
            case SENSOR_2: 
                        hw_Alarma();
                        
                    
                    break;

            default:
                    printf("Sensor inexistente \n\n");
                    break;
        }
       hw_Pausems(2000); 

    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
