#ifndef _CAFETERA_H_
#define _CAFETERA_H_

/*==================[inclusions]=============================================*/

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

typedef enum {
    ESPERANDO_FICHA,
    SELECCIONANDO_INFUSION,
    SIRVIENDO_TE,
    SIRVIENDO_CAFE,
    ALARMA
} STATES_CAFETERA;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

void fsm_cafetera_init(void);
void fsm_cafetera_runCycle(void);

void fsm_cafetera_ev_Ficha(void);
void fsm_cafetera_ev_select_Te(void);
void fsm_cafetera_ev_select_Cafe(void);
void fsm_cafetera_ev_tick1seg(void);
void fsm_cafetera_ev_tick100mseg(void);
void fsm_cafetera_ev_tick500mseg(void);
void fsm_cafetera_printCurrentState(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _GARAJE_H_ */