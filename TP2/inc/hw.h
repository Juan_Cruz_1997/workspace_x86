#ifndef _HW_H_
#define _HW_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>

/*==================[macros]=================================================*/

#define EXIT   27  // ASCII para la tecla Esc 
#define FICHA  49  // ASCII para la tecla 1 
#define TE     50  // ASCII para la tecla 2 
#define CAFE   51  // ASCII para la tecla 3

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

void hw_Init(void);
void hw_DeInit(void);
void hw_Pausems(uint16_t t);
uint8_t hw_LeerEntrada(void);
void hw_devolver_ficha(void);
void hw_eregar_Te(void);
void hw_eregar_Cafe(void);
void hw_activarAlarma(void);
void hw_apagarAlarma(void);
void hw_infusion_terminada(void);
/*==================[end of file]============================================*/
#endif /* #ifndef _HW_H_ */
