/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"
#include "fsm_cafetera.h"
#include <stdio.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input = 0;
    uint16_t cont_1s = 0;
    uint16_t cont_100ms = 0;
    uint16_t cont_500ms = 0;

    hw_Init();

    fsm_cafetera_init();

    printf ("Presione 1 para ingresar una ficha\n\n");
    printf ("Presione 2 si quiere Te o presione 3 si quiere Cafe\n\n");
    while (input != EXIT) {
        input = hw_LeerEntrada();

        if (input == FICHA) {
            fsm_cafetera_ev_Ficha();
        }

        if (input == TE ){
            fsm_cafetera_ev_select_Te();
        }
        
        if (input == CAFE ){
            fsm_cafetera_ev_select_Cafe();
        }
        cont_1s ++;
        
        if (cont_1s == 1000) {
            cont_1s = 0;
            fsm_cafetera_ev_tick1seg();
        }
        cont_100ms++;
        
        if (cont_100ms == 100) {
            cont_100ms = 0;
            fsm_cafetera_ev_tick100mseg();
        }
        cont_500ms ++;
        
        if (cont_500ms == 100) {
            cont_500ms = 0;
            fsm_cafetera_ev_tick500mseg();
        }

        fsm_cafetera_runCycle();
        hw_Pausems(1);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/