/*==================[inclusions]=============================================*/

#include "fsm_cafetera.h"
#include "hw.h"
#include <stdio.h>
#include <stdbool.h>

/*==================[macros and definitions]=================================*/

#define TIEMPO_SELECCION  30
#define EROGAR_TE 30000
#define EROGAR_CAFE 45000
#define TIEMPO_ALARMA 2
/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

STATES_CAFETERA state;

uint8_t ev_Ficha;
uint8_t ev_select_Te;
uint8_t ev_select_Cafe;
uint8_t ev_tick1seg;
uint8_t ev_tick100mseg;
uint8_t ev_tick500mseg;
uint16_t count_seg;
uint16_t count_100mseg;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void clearEvents(void)
{
    ev_Ficha = 0;
    ev_Ficha = 0;
    ev_select_Te = 0; 
    ev_select_Cafe = 0;
    ev_tick1seg = 0;
    ev_tick100mseg = 0;
    ev_tick500mseg = 0;
}

/*==================[external functions definition]==========================*/

void fsm_cafetera_init(void)
{
    state = ESPERANDO_FICHA;
    clearEvents();
}

void fsm_cafetera_runCycle(void)
{
    // El diagrama se encuentra en fsm_garaje/media/fsm_garaje_diagrama.png
    switch (state) {
        case ESPERANDO_FICHA:
            if (ev_Ficha) {
                
                count_seg = 0;
                state = SELECCIONANDO_INFUSION;
            }
            else if (ev_tick500mseg){
                //toggleLED();
            }
            break;

        case SELECCIONANDO_INFUSION:
            
            if (ev_select_Te) {
                count_100mseg = 0;
                hw_eregar_Te();
                state = SIRVIENDO_TE;
            }
            else if(ev_select_Cafe){
                count_100mseg = 0;
                hw_eregar_Cafe();
                state = SIRVIENDO_CAFE;
            }
            else if (ev_tick1seg && (count_seg < TIEMPO_SELECCION )){
                printf("Seleccione una infusion (2) Te (3) Cafe\n\n");
                count_seg ++;
            }
            else if (ev_tick1seg && (count_seg == TIEMPO_SELECCION )){
                hw_devolver_ficha();
                state = ESPERANDO_FICHA;
            }
            break;

        case SIRVIENDO_TE:
            if (ev_tick100mseg && (count_100mseg < EROGAR_TE)) {
            
                count_100mseg += 100;
                //toggleLED();
            }
            else if (ev_tick100mseg && (count_100mseg == EROGAR_TE)) {
                hw_activarAlarma();
                count_seg = 0;
                state = ALARMA;
            }
            break;
        
        case SIRVIENDO_CAFE:
            if (ev_tick100mseg && (count_100mseg < EROGAR_CAFE)) {
                count_100mseg += 100;
                //toggleLED();
            }
            else if (ev_tick100mseg && (count_100mseg == EROGAR_CAFE)) {
                count_seg = 0;
                hw_activarAlarma();
                state = ALARMA;
            }
            break;

        case ALARMA:
            if (ev_tick1seg && (count_seg < TIEMPO_ALARMA)) {
                count_seg ++;
            }
            else if (ev_tick1seg && (count_seg == TIEMPO_ALARMA)) {
                hw_infusion_terminada();
                hw_apagarAlarma();
                state = ESPERANDO_FICHA;
            }
            break;
    }

    clearEvents();
}

void fsm_cafetera_ev_Ficha(void)
{
    ev_Ficha = 1;
}

void fsm_cafetera_ev_select_Te(void)
{
    ev_select_Te = 1;
}

void fsm_cafetera_ev_select_Cafe(void)
{
    ev_select_Cafe = 1;
}
void fsm_cafetera_ev_tick1seg(void)
{
    ev_tick1seg = 1;
}
void fsm_cafetera_ev_tick100mseg(void)
{
    ev_tick100mseg = 1;
}

void fsm_cafetera_ev_tick500mseg(void)
{
    ev_tick500mseg = 1;
}

void fsm_cafetera_printCurrentState(void)
{
    printf("Estado actual: %0d \n", state);
}

/*==================[end of file]============================================*/