/*==================[inclusions]=============================================*/

#include "hw.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

struct termios oldt, newt;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void hw_Init(void)
{
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);       
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

void hw_DeInit(void)
{
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);    
}

void hw_Pausems(uint16_t t)
{
    usleep(t * 1000);
}

uint8_t hw_LeerEntrada(void)
{
    return getchar();
}

void hw_devolver_ficha(void)
{
    printf("Devolviendo Ficha \n\n");
}

void hw_eregar_Te(void)
{
    printf("Sirviendo Te \n\n");
}

void hw_eregar_Cafe(void)
{
    printf("Sirviendo Cafe \n\n");
}

void hw_activarAlarma(void)
{
    printf("Alarma Activada \n\n");
}

void hw_apagarAlarma(void)
{
    printf("Alarma desactivada \n\n");
}

void hw_infusion_terminada(void)
{
    printf("Bebida servida \n\n");
}
/*==================[end of file]============================================*/
